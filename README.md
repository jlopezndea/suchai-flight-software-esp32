# suchai-flight-software-esp32


## Como replicar lo visto en los correos 
1. Instalar idf siguiendo los pasos del siguiente link [https://docs.espressif.com/projects/esp-idf/en/latest/esp32/get-started/linux-macos-setup.html] hasta llegar al hello world
2. Clonar este repositorio con el nombre de "hello_world" en el mismo nivel de la carpeta del esp-idf
3. Ir a 'components/suchai/' y ejecutar runme.sh
4. Deberia de funcionar ahora si ejecuta get_idf y usa 'idf.py build'/ si usa CLion y deja que configure el cmake.

## Cambios importantes! 
Puede encontrar los cambios al suchai-flight-software en [https://gitlab.com/jlopezndea/suchai_32] pero a grandes rasgos fue añadir al cmake los directorios de los includes, cambiar los 'FreeRTOS' a 'freertos/FreeRTOS', desactivar 'lineoise' ya que no reconocia "struct winsize". notar que es mas comodo agregar en el cmake del componente "components/suchai/CMakeLists"
en vez de "suchai-flight-software/src" solo coloca "suchai-flight-software" y se compila lo del directorio  apps tambien.

Ahora, notar que esta fallando porque esta intentando agregar libcsp.a pero no conoce los simbolos.(Creo)
